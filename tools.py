# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.
from config import *
import shlex,subprocess,sys

def fastq_interleave(fname1,fname2,fnameout=None):
    if not fnameout:
        fout = sys.stdout
    else:
        fout = open(fnameout,"w")

    with open(fname1) as f1:
        with open(fname2) as f2:
            while True:
                l1 = f1.readline()
                if not l1:
                    break
                fout.write(l1)
                fout.write(f1.readline())
                fout.write(f1.readline())
                fout.write(f1.readline())
                fout.write(f2.readline())
                fout.write(f2.readline())
                fout.write(f2.readline())
                fout.write(f2.readline())

    if fnameout:
        fout.close()

def split_list(l,num_pieces):
    """ split a list into pieces passed as param """
    start = 0
    for i in range(num_pieces):
        stop = start + len(l[i::num_pieces])
        if len(l[start:stop]) == 0:
            break
        yield l[start:stop]
        start = stop

def download(q,files,destdir,i,log):
    if len(files) > 1:
        log.info("Starting download thread #{0} ({1} files)".format(i,len(files)))
    else:
        log.info("Starting download thread #{0} ({1})".format(i,files[0]))

    for f in files:
        command = CMD_WGET.format(destdir,f)
        args = shlex.split(command)
        p = subprocess.call(args,stdout=subprocess.PIPE)
    
    log.info("Finished download thread #{0}".format(i))

def download_one(f,destdir,newname=None):
    command = CMD_WGET.format(destdir,f)
    if newname:
        command += " -O {0}/{1}".format(destdir,newname)
    args = shlex.split(command)
    p = subprocess.call(args,stdout=subprocess.PIPE)


class PathTracker(object):
    def __init__(self):
        self.__levels = []

    def pop(self):
        tmp = self.__levels[-1]
        del self.__levels[-1]
        return tmp

    def push(self,level):
        self.__levels.append(level)

    def stringify(self):
        return ">".join(self.__levels)

    def currLevel(self):
        return self.__levels[-1]

def get_uniprot_entry(acc):
    '''
        Gets an entry from uniprot, or a suitable replacement if possible.

        acc: the accession number of the entry to search for

        returns a list of SeqRecords containing [1..n]
    '''
    import urllib2
    from Bio import SeqIO
    
    try:
        theurl = "http://www.uniprot.org/uniprot/{0}.fasta".format(acc)
        funientry = urllib2.urlopen(theurl)
    except urllib2.HTTPError:
        theurl = "http://www.uniprot.org/uniprot/?query=replaces:{0}&format=fasta".format(acc)
        funientry = urllib2.urlopen(theurl)

    unirecords = SeqIO.parse(funientry, "fasta")

    return(unirecords)

def search_uniprot(query,format="fasta"):
    '''
        Gets a list of entries from uniprot.

        query: the uniprot text query
        format: the format of the records to obtain

        returns a list of SeqRecords
    '''
    import urllib2
    from Bio import SeqIO
    
    theurl = "http://www.uniprot.org/uniprot/?query={0}&force=yes&format={1}".format(query,format)
    funientry = urllib2.urlopen(theurl)

    if format == "txt":
        bformat = "swiss"

    unirecords = SeqIO.parse(funientry, bformat)

    return(unirecords)

def split_len(seq, length):
    return [seq[i:i+length] for i in range(0, len(seq), length)]

def splitlist(listlen,start,step):
    limits = range(start,listlen,step)
    limits.append(listlen)
        
    result = []

    for i in xrange(0, len(limits)-1):
        lim0 = limits[i]
        lim1 = limits[i+1]

        if(lim0 == start):
            lim0 -= 1

        result.append((lim0+1,lim1))    

    return result

def three2one(prot,lst=False):                                            
    code = {
      'ALA' : 'A', 'VAL' : 'V', 'LEU' : 'L', 'ILE' : 'I', 'PRO' : 'P',
      'TRP' : 'W', 'PHE' : 'F', 'MET' : 'M', 'GLY' : 'G', 'SER' : 'S',
      'THR' : 'T', 'TYR' : 'Y', 'CYS' : 'C', 'ASN' : 'N', 'GLN' : 'Q',
      'LYS' : 'K', 'ARG' : 'R', 'HIS' : 'H', 'ASP' : 'D', 'GLU' : 'E',
    }

    if lst:
        return [code.get(aa,"X") for aa in prot]
    else:
        return "".join([code.get(aa,"X") for aa in prot])

def hydrophobicity(seq):
    hydros = { #hydrophobicity values per aa
        'A' :  1.800, 'C' :  2.500, 'D' : -3.500, 'E' : -3.500, 'F' :  2.800,
        'G' : -0.400, 'H' : -3.200, 'I' :  4.500, 'K' : -3.900, 'L' :  3.800,
        'M' :  1.900, 'N' : -3.500, 'P' : -1.600, 'Q' : -3.500, 'R' : -4.500,
        'S' : -0.800, 'T' : -0.700, 'V' :  4.200, 'W' : -0.900, 'Y' : -1.300
    }

    hydrosn = {} #will contained normalized hydrophob values

    vals = [hydros[k] for k in hydros]
    mx = max(vals) #max and min hydrophob values, for the normalisation
    mn = min(vals)

    hydrosn = {h : (hydros[h] + abs(mn))/(mx-mn) for h in hydros} #normalise the hydros dict

    h = 0
    for aa in seq:
        h += hydrosn[aa]

    return h

def netcharge(seq): #calculate net charge for a seq
    charges = {"C":-.045,"D":-.999,"E":-.998,"H":.091 ,"K":1,"R":1,"Y":-.001}
    charge = -0.002

    for aa in seq:
      if aa in charges:
         charge=charge+charges[aa]

    return abs(charge)
