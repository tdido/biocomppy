# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.

import itertools

class Slice:

    def __init__(self,slistr):
        self.__string = slistr
        self.patterns = []

    def __eq__(self,other):
        return(self.__string == other.string)

    def __hash__(self):
        return(hash(self.__string))

    def genpatterns(self):
        char_options = [self.__string[i]+'X' for i in xrange(1,len(self.__string)-1)]
        char_options.insert(0,self.__string[0])
        char_options.append(self.__string[-1])

        self.patterns = ["".join(p) for p in itertools.product(*char_options)]

    def genchildren(self):
        self.genpatterns()

class Region:

    def __init__(self,regstr,start=None,end=None):
        self.start = start
        self.end = end
        self.__string = regstr
        self.slices = []
        self.slicesstr = []

    def genslices(self,slen):
        if(len(self.__string) >= slen):
            i = 0
            while(i+slen <= len(self.__string)):
                if(len(self.__string[i:i+slen]) >= slen):
                    s = Slice(self.__string[i:i+slen])
                    self.slices.append(s)
                    self.slicesstr.append(s.string)
                i+=1

    def genuniqueslices(self):
        self.uniqueslices = set([s for s in self.slices])

    def genchildren(self,slen):
        self.genslices(slen)
        for s in self.slices:
            s.genchildren()

class BioSeq:

    def __init__(self,seqstr,id=None,alphabet='ACDEFGHIKLMNPQRSTVWYX',gapchars="-"):
        self.__id = id
        self.__alphabet=alphabet
        self.__string = str(seqstr)
        self.__regions = {} 

    def get_string(self):
        return self.__string

    def genchildren(self,rlabel,template,slen):
        self.genregions(rlabel,template,marker='1')
        for r in self.__regions[rlabel]:
            r.genchildren(slen)

    def getslices(self,rlabel):
        '''get all the slices for all the regions in this sequence'''
        res = []
        for r in self.__regions[rlabel]:
            for s in r.slices:
                res.append(s)

        return(res)

    def genregions(self,label,template,marker='1'):
        '''split the region by a template and add the regions to a list in the "regions" dictionary, under the key "label"'''

        regions = self.splitbytemplate(template,marker)[0] #only interested in the list of regions marked by "marker"
        self.__regions[label] = [Region(reg[2],reg[0],reg[1]) for reg in regions]

    def genspmutants(self,ranges=None,chars=None,offset=0,skippos=[]):
        '''Generate single point mutants from the sequence
        
        ranges: a list of the type [(startpos,endpos),(startpos2,endpos2)] tuples of position ranges to mutate (defaults to all residues).
        chars: chars to use as mutations (defaults to IUPAC's 20 residues).

        returns a list of tuples of the type (mutated_pos,orig_res,new_res,mutant_seq)
        '''
        if not chars:
            chars = self.__alphabet

        if ranges == None:
            ranges = [(1,len(self.__string))]

        mutpos = [];
        mutseqs = [];
    
        for range in ranges:
            for pos in xrange(range[0]-1,range[1]):
                mutpos.append(pos)

        mutpos = set(mutpos)

        for pos in mutpos:
            if pos not in skippos:
                for newchar in chars:
                    curchar = self.__string[pos]
                    if curchar != newchar:
                        mutseqs.append((pos+1+offset,curchar,newchar,self.__string[:pos]+newchar+self.__string[pos+1:]))
    
        return(mutseqs)

    @staticmethod
    def compare(seq1,seq2,gapmarker='-',gapsmatch=False,require_one_ann=False):
        '''
        Calculate the percentage to which two strings match, allowing for different counting strategies and for gap marker handling.

        seq1 (string): the second string to compare
        seq2 (string): the second string to compare
        gapmarker (char): the marker to be considered as gap
        gapsmatch (string): wether to count as match when there are gaps in both sequences

        Returns a set of the form (pct_match,matches,length)
        '''
        if len(seq1) != len(seq2):
            raise ValueError("Sequences to compare have different lengths")

        nmatches = 0
        length = 0

        for i in xrange(0,len(seq1)):
            if require_one_ann:
                if seq1[i] != gapmarker or seq2[i] != gapmarker: 
                    length += 1
            else:
                length += 1

            if (seq1[i] != gapmarker or seq2[i] != gapmarker or gapsmatch) and seq1[i] == seq2[i]:
                nmatches += 1

        if length == 0:
            return((0,0,0))
        else:
            return((nmatches/float(length),nmatches,length))

    def splitbytemplate(self,template):
        '''Splits a string into regions based on a template of the same length.
            Returns a dictionary where key=marker and value=a list of tuples of the type (startpos,endpos,'seq'), representing regions of consecutive equal markers.'''

        if(self.__string == None or template == None):
            return(([],[]))

        curchar=""
        limits = []

        for i in xrange(0,len(template)):
            if (curchar != template[i]):
                limits.append(i)
                curchar = template[i]

        limits.append(len(self.__string))

        markers = set(template)
        slices = {}

        for m in markers:
            slices[m] = [] 

        prevlim = 0
        for lim in limits:
            if lim > 0:
                for m in slices:
                    if template[prevlim] == m:
                        slices[m].append((prevlim+1,lim,self.__string[prevlim:lim]))
        
                prevlim = lim

        return(slices)

    def alignsubseq(self,subseq,gapchar="-",guess=False):
        '''Align a sub sequence (provided as a list of regions and their starting positions ((21,'AASASASD')) to its supersequence, filling the gaps with "gapchar"
           If the parameter guess=True, it will try to guess the position by searching the string for those regions with position = None'''
        newseq = ""

        for i in self.__string:
            newseq += gapchar 

        for region in subseq:
            regseq = region[1]
            regpos = region[0]
            if(guess and regpos == None):
                idx = self.__string.find(regseq)
            else:
                idx = regpos-1
            newseq = newseq[0:idx] + regseq + newseq[idx+len(regseq):len(self.__string)]
       
        return(newseq)

    @staticmethod
    def expandwithgaps(seq,target,gapchar="-"):
        '''Expand the target sequence with the gaps from the active sequence
        '''
        idxtarget = 0
        tmptarget = ""
        for idx in xrange(0,len(seq)):
            if seq[idx] == gapchar:
                tmptarget += gapchar
            else:
                tmptarget += target[idxtarget]
                idxtarget += 1

        return(tmptarget)

    @staticmethod
    def gapremove(refseq, tarseq, gapchar="-"):
        '''Remove the gaps from the reference sequence and the corresponding positions in the target sequence'''
        newrefseq=""
        newtarseq=""
        
        for i in xrange(0,len(refseq)):
            if(refseq[i] != gapchar):
                newrefseq += refseq[i]
                newtarseq += tarseq[i]

        return (newrefseq,newtarseq)

    @staticmethod
    def align(seq1, seq2, clustalw_exe=r"./clustalw2"):
        import tempfile,os,subprocess
        from Bio.Align.Applications import ClustalwCommandline
        from Bio import AlignIO

        tmpfile = tempfile.NamedTemporaryFile(delete=False)
        tmpfilename = tmpfile.name
        tmpfile.write(">a\n%s\n>b\n%s\n" % (seq1, seq2))
        tmpfile.close()
    
        clustalw_cline = ClustalwCommandline(clustalw_exe, infile=tmpfilename)
        clustalw_cline.set_parameter("outorder","input")
        try:
            stdout, stderr = clustalw_cline() #biopython 1.55 and above
        except TypeError:
            subprocess.call(str(clustalw_cline).split())
    
        align = AlignIO.read(tmpfilename + ".aln", "clustal")
    
        seq1 = str(align[0].seq)
        seq2 = str(align[1].seq)

        os.remove(tmpfilename)
        os.remove(tmpfilename+".aln")
        os.remove(tmpfilename+".dnd")

        return (seq1,seq2)

    def annotate(self, seqann, template, align=True, expandtemplate=True, simple=False, gapchar="-",marker="1"):
        '''
            Annotates a reference sequence with an annotation sequence and its corresponding binary annotation template, and returns the annotation string.
            The annotation coding is as follows (read by columns):

                self.__string:  ?  ?  -  -  X  X  X  X
                seqann:       -  ?  X  X  Y  Y  X  X
                template:     ?  -  0  1  0  1  0  1
                              ----------------------
                result:       -  -  +  *  o  l  0  1
                
                Where ? is anything, X is a symbol and Y is a symbol other than X.
    
            seqann: the annotation source sequence
            template: the annotating template, obtained from seqann
            align: if true, align both sequences before annotation
            expandtemplate: if true, expand the template with the gaps from seqann
            simple: if true, only use 0/1 as annotation symbols
            gapchar: the gap character to be used
            marker: the positive (binary) annotation marker to use
        '''

        if align:
            seqref, seqann = self.align(seqann)
        else:
            seqref = self.__string

        if expandtemplate:
            tmpbseq = BioSeq(seqann)
            template = tmpbseq.expandwithgaps(template)

        anntmp=""
        
        for i in xrange(0,len(seqann)):
            if seqann[i] == gapchar:
                anntmp += gapchar 
            elif seqref[i] == gapchar:
                if template[i] == marker:
                    anntmp += "*"
                else:
                    anntmp += "+"
            else:
                if seqann[i] == seqref[i]:
                    anntmp += template[i]
                else:
                    if template[i] == marker:
                        anntmp += "l"
                    elif template[i] == gapchar:
                        anntmp += gapchar
                    else:
                        anntmp += "o"

        if simple:
            anntmp = anntmp.replace("o","0").replace("l","1").replace("+",gapchar).replace("*",gapchar)

        return (seqref,seqann,anntmp)

    @staticmethod
    def get_regions_maps(seq1,seq2,seq1_offset=0,seq2_offset=0):
        seq1p = 0
        seq2p = 0
        subregstart = None

        regmaps = []

        for i in xrange(0,len(seq1)):
            if seq1[i] == "-" or seq2[i] == "-":
                if subregstart:
                    seq2regs = seq2_offset + subregstart[0] - 1
                    seq1regs = seq1_offset + subregstart[1]
                    seq2rege = seq2regs + (seq2p - subregstart[0])
                    seq1rege = seq1p
                    tmpmap = [(seq1regs,seq1rege),(seq2regs,seq2rege)]
                    regmaps.append(tmpmap)

                    subregstart = None
            else:
                if subregstart == None:
                    subregstart = [seq2p+1,seq1p+1]

            if seq1[i] != "-":
                seq1p += 1

            if seq2[i] != "-":
                seq2p += 1

        if subregstart:
            seq2regs = seq2_offset + subregstart[0] - 1
            seq1regs = seq1_offset + subregstart[1]
            seq2rege = seq2regs + (seq2p - subregstart[0])
            seq1rege = seq1p
            tmpmap = [(seq1regs,seq1rege),(seq2regs,seq2rege)]
            regmaps.append(tmpmap)

        return regmaps

class BioSeqList:
    def __init__(self,seqlist):
        self.seqs = [BioSeq(seqstr) for seqstr in seqlist]
