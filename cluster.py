# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.

class CDHITWrapper:
    def __init__(self,cdhbin="/usr/local/bin/cd-hit"):
        self.__cdhitbin = cdhbin

    def getrepseqs(self,clstrfile,scoresdict):
        '''get a list of the highest scoring sequence for each cluster, based on a dictionary of scores per sequence'''

        self.__scoresdict = scoresdict
        self.__clstrsfile = clstrfile

        self.__buildclustersdict()

        repseqs = []

        for key in self.__clstrs:
            maxscore = None
            bestseq = ""
            for seq in self.__clstrs[key]:
                if maxscore is None or self.__scoresdict[seq] > maxscore:
                    maxscore = self.__scoresdict[seq]
                    bestseq = seq
            repseqs.append(bestseq)

        return(repseqs)

    def __buildclustersdict(self):
        '''Get a dictionary of clusters from a CD-HIT clstr file'''

        if self.__clstrsfile is not file:
            self.__clstrsfile = open(self.__clstrsfile, "rU")

        clstrs = {}

        for l in self.__clstrsfile:
            if(l[0] == ">"):
                currclust = l.rstrip("\n").split(" ")[1]
                clstrs[currclust] = []
            else:
                seq = l.split(">")[1].split(".")[0]
                clstrs[currclust].append(seq)

        self.__clstrs = clstrs
