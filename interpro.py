# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.

import xml.parsers.expat
import sys,re,json
from biocomppy.tools import XMLPathTracker

def start_element(name, attrs):
    global usrdata

    if name != 'interprodb':
        usrdata["path"].push(name)

    currpath = usrdata["path"].stringify()
    currlev = name

    if currpath == "interpro":
        id = attrs["id"]
        usrdata["entry"] = {"id":id,"db_xrefs":{}}
    elif currpath == "interpro>class_list>classification" and attrs["class_type"] == "GO":
        try:
            usrdata["entry"]["go"].append(attrs["id"])
        except KeyError:
            usrdata["entry"]["go"] = [attrs["id"]]
    elif currlev == "db_xref":
        try:
            usrdata["entry"]["db_xrefs"][attrs["db"]].append(attrs["dbkey"])
        except KeyError:
            usrdata["entry"]["db_xrefs"][attrs["db"]] = [attrs["dbkey"]]

def char_data(data):
    global usrdata

    currpath = usrdata["path"].stringify()

    if currpath == "interpro>name":
        usrdata["entry"]["name"] = data

def end_element(name):
    global usrdata

    if name == 'interpro':
        print(json.dumps(usrdata["entry"]))

    if name != 'interprodb':
        usrdata["path"].pop()

class InterProParser(object):

    def __init__(self):
        pass

    def parse(self,filepath):
        global usrdata

        usrdata = {"path":XMLPathTracker()}

        p = xml.parsers.expat.ParserCreate()

        p.StartElementHandler = start_element
        p.EndElementHandler = end_element
        p.CharacterDataHandler = char_data

        with open(filepath) as f:
            p.ParseFile(f)
