# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.
from config import *
from tools import download, split_list,three2one
import logging,os,sys

try:
    import simplejson as json
except ImportError:
    import json

class PDBUpdater:
    def __init__(self,numthreads=4):

        classname = "PDBUpdater"
        logging.basicConfig(filename=FILE_LOG,level=logging.DEBUG,format=LOG_FORMAT.format(classname))
        self.log = logging.getLogger(classname)
        self.log.info("Initializing")
        self.__numthreads = numthreads

    def update_derived(self):
        from multiprocessing import Process, Queue

        q = Queue()

        i = 0

        for files in split_list(URLS_PDB_DERIVED,self.__numthreads):
            p = Process(target=download, args=(q,files,PATH_PDB_DERIVED,i,self.log))
            p.start()
            i+=1

class PDB2JSON(object):
    def __init__(self):
        from sifts import SiftsParser

        self.__siftsparser = SiftsParser()

        self.__entries = {}
        with open("{0}/entries.idx".format(PATH_PDB_DERIVED)) as f:
            f.readline()
            f.readline()
            for r in f:
                row = r.rstrip("\n").split("\t")
                pdbid = row[0].lower()
                self.__entries[pdbid] = {}
                e = self.__entries[pdbid]
                e["compound"] = row[3].strip()
                e["id"] = pdbid
                if row[4]:
                    e["org"] = [t.strip() for t in row[4].split(";")]
                e["res"] = [None if res.strip() == "NOT" else float(res.strip()) for res in row[6].split(",")]
                e["xtype"] = [t.strip() for t in row[7].split(",")]
                e["ch"] = {}

    def generate_files(self,entries_filename,links_filename):
        with open(entries_filename,"w") as entries_file:
            with open(links_filename,"w") as links_file:
                for sifts_file in os.listdir(PATH_SIFTS_XML):
                    try:
                        self.process_entry(sifts_file,entries_file,links_file)
                    except ValueError:
                        print("skipping {0}".format(sifts_file))
                        print(sys.exc_info())

    def process_entry(self,sifts_file,entries_file,links_file):
        try:
            sentry = self.__siftsparser.parse("{0}/{1}".format(PATH_SIFTS_XML,sifts_file))
        except:
            raise ValueError("Error parsing {0}".format(sifts_file)) 

        try:
            entry = self.__entries[sentry.id]
        except KeyError:
            raise ValueError("Entry not found. Probably superseded.") 

        linksd = {} # store the link information of each chain for later usage

        for ent in sentry.entities: #don't use the entity annotations to split by chains, because weird things may happen in sifts files. Use the chain attribute at residue level
            for seg in ent.segments:
                for res in seg.residues:
                    residue = {"aa":res.pdbresname,"n":res.pdbresnum,"p":res.naturalpos}

                    if res.notobserved:
                        residue["nob"] = True

                    if res.conflict:
                        residue["con"] = True

                    if res.modified:
                        reside["mod"] = True

                    if res.ss:
                        residue["ss"] = res.ss

                    try:
                        entry["ch"][res.chainid]["res"].append(residue)
                    except KeyError:
                        entry["ch"][res.chainid] = {"res": [residue]}
                    finally:
                        if res.uniprotpos: #if we have a link to a uniprot position for this residue
                            try:
                                linksd[res.chainid][res.uniprotaccession].append([res.uniprotpos,res.pdbresnum])
                            except KeyError:
                                try:
                                    linksd[res.chainid][res.uniprotaccession] = [[res.uniprotpos,res.pdbresnum]]
                                except KeyError:
                                    linksd[res.chainid] = {res.uniprotaccession: [[res.uniprotpos,res.pdbresnum]]}

        self.__res_list_to_dict(entry)

        for chain in linksd:
            for uacc in linksd[chain]:
                linksentry = {"racc":uacc,"rsrc":"uniprot","aacc": "{0}_{1}".format(entry["id"],chain),"asrc":"pdb"}
                linksentry["l"] = linksd[chain][uacc]
                links_file.write(json.dumps(linksentry) + "\n")

        entries_file.write(json.dumps(entry) + "\n")

    def __res_list_to_dict(self,entry):
        '''Converts the residues list in each pdb entry chain to a residues dictionary, with pdbresnums as keys.'''

        def no_n(r):
            del r["n"]
            return r

        for chain in entry["ch"]:
            d = entry["ch"][chain]["res"]
            entry["ch"][chain]["res"] = dict((r["n"],no_n(r)) for r in d)
            #entry["ch"][chain]["res"] = {r["n"]:no_n(r) for r in d} #python >= 2.7

def generate_graphic(pdbid, chainid=None, width=1024, outputpath=".", filename=None, rotate=False):
    import __main__
    __main__.pymol_argv = [ 'pymol', '-qc'] # Quiet and no GUI

    try: import pymol
    except ImportError: raise ImportError("PyMOL is required for graphics generation, and could not be found.")

    import time,sys,os
     
    pymol.finish_launching()

    '''
    Function to generate a cartoon representation from a PDB structure in PNG format.
    
    pdbid: the PDB id of the structure to print
    chainid: the id of the chain to print (default=all chains)
    width: the width of the output image (default:1024px)
    outputpath: the path where to save the image (default:working dir)
    '''
    url = "http://www.pdb.org/pdb/files/{0}.pdb".format(pdbid)
    if filename:
        image_name = "{0}/{1}".format(outputpath,filename)
    else:
        if chainid:
            image_name = "{0}/{1}{2}".format(outputpath,pdbid,chainid)
        else:
            image_name = "{0}/{1}".format(outputpath,pdbid)

    try:
        with open(image_name): pass
        os.remove(image_name)
    except IOError:
        pass

    pymol.cmd.load(url,pdbid)
    pymol.cmd.orient()
    pymol.cmd.hide("all")

    if chainid:
        pymol.cmd.show("cartoon","chain {0}".format(chainid))
        pymol.cmd.center("chain {0}".format(chainid))
        pymol.cmd.zoom("chain {0}".format(chainid))
    else:
        pymol.cmd.show("cartoon")
        pymol.cmd.center()
        pymol.cmd.zoom()

    pymol.util.chainbow(pdbid)
    time.sleep(0.1)

    if rotate:
        pymol.cmd.png(image_name+"-1",width,False)
        time.sleep(0.1)
        pymol.cmd.rotate("y",90);
        pymol.cmd.png(image_name+"-2",width,False)
        time.sleep(0.1)
        pymol.cmd.rotate("y",90);
        pymol.cmd.png(image_name+"-3",width,False)
        time.sleep(0.1)
        pymol.cmd.rotate("x",90);
        pymol.cmd.png(image_name+"-4",width,False)
        time.sleep(0.1)
    else:
        pymol.cmd.png(image_name,width,False)
        time.sleep(0.1)

    pymol.cmd.remove(pdbid)

    return pymol

'''
import PythonMagick

while True:
    try:
        with open(image_name):
            image = PythonMagick.Image(image_name)
            image.trim()
            image.write("b"+image_name)
            break
    except IOError:
        time.sleep(1)
'''
def parsearguments():
    import argparse,sys

    description = """MobiDB loader"""
    epilog = """Alea jacta est."""
    parser = argparse.ArgumentParser(description=description, epilog=epilog,formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--update-derived',action='store_true',help="update derived files",default=False)
    parser.add_argument('--generate-files',action='store_true',help="load pdb",default=False)
    parser.add_argument('--output-path',type=str,help='output path for the generated files',default='.')
    parser.add_argument('--nthreads',type=int,help='number of threads to use',default=1)

    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)

    return(vars(parser.parse_args()))

if __name__ == "__main__":
    args = parsearguments()

    if args["update_derived"]:
        u = PDBUpdater(numthreads=args["nthreads"])
        u.update_derived()

    if args["generate_files"]:
        print("*******************************************************")
        print("WARNING: Remember to update the sifts files before this")
        print("*******************************************************")

        l = PDB2JSON()
        l.generate_files("{0}/entries.json".format(args["output_path"]),"{0}/links.json".format(args["output_path"]))
