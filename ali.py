# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.

from Bio import AlignIO
from Bio.Align import MultipleSeqAlignment
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

class BioAli:

    def __init__(self):
        self.__alignment = MultipleSeqAlignment([])
        self.__nseqs = 0
        self.__alen = None

    def load_alignment(self,filename,format="fasta"):
        self.__format = format
        self.__alignment = AlignIO.read(filename, self.__format)
        self.__alen = self.__alignment.get_alignment_length()
        self.__nseqs = len(self.__alignment)

    def add_sequence(self,sequence):
        self.__alignment.append(SeqRecord(Seq(sequence)))
        self.__nseqs += 1
        self.__alen = len(sequence)

    def get_res_freqs(self,ignore=[]):
        '''Get residue frequences by position from a MSA.

        ignore: list of characters to ignore (e.g. "-" for gaps)

        Returns a list containing a dictionary for each position in the MSA: e.g.: {'A':0.5,'N':0.5} meaning 50% frequency for N and A.'''

        self.__freqdict = []

        for pos in xrange(0,self.__alen): # init dictionary
            self.__freqdict.append({})

        for seq in self.__alignment:
            for pos in xrange(0,self.__alen):
                if seq[pos] in ignore:
                    continue

                try:
                    self.__freqdict[pos][seq[pos]] += 1
                except KeyError:
                    self.__freqdict[pos][seq[pos]] = 1

        for pos in self.__freqdict:
            for res in pos:
                pos[res] = pos[res]/float(self.__nseqs)

        return(self.__freqdict)

    def get_consensus_seq(self,gapchar="-",ignore=[]):
        '''Calculates a consensus sequence based on residue frequency.

        ignore: list of characters to ignore in the consensus (e.g. "-" for gaps)

        Returns a consensus sequence (string)'''

        try:
            self.__freqdict
        except:
            self.__get_res_freqs(ignore=ignore)

        conseq = ""

        for pos in self.__freqdict:
            try:
                conseq += max(pos, key=pos.get)
            except ValueError:
                conseq += gapchar

        return(conseq)


    def get_conservation_seq(self,threshold=0.5,noncmarker="0",cmarker="1"):
        '''just for backward compatibility'''
        return(get_bin_cons_seq(self,threshold=0.5,noncmarker="0",cmarker="1"))

    def get_bin_cons_seq(self,threshold=0.5,noncmarker="0",cmarker="1"):
        '''Calculates the conservation sequence based on AA frequency for a give MSA.

        freqdict: frequency dictionary as obtained from "getresfreqs()".
        
        Returns a sequence (string) composed of the chosen markers (default 0/1) indicating conserved/non-conserved residues.'''

        try:
            self.__freqdict
        except:
            self.__get_res_freqs()

        conseq = ""

        for pos in self.__freqdict:
            m = max(pos, key=pos.get)
            if pos[m] < threshold or m == '-':
                conseq += noncmarker
            else:
                conseq += cmarker

        return(conseq)

    def get_cons(self):
        '''Calculates conservation values for each position and returns a list containing them'''

        try:
            self.__freqdict
        except:
            self.__get_res_freqs()

        conslist = []

        for pos in self.__freqdict:
            m = max(pos, key=pos.get)
            if m == '-':
                conslist.append(0)
            else:
                conslist.append(pos[m])

        return(conslist)
