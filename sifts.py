# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.

from lxml import etree as ET
from config import *
import gzip,logging
from tools import download, split_list

class SiftsUpdater:
    def __init__(self,numprocs=4):

        classname = "SiftsUpdater"
        logging.basicConfig(filename=FILE_LOG,level=logging.DEBUG,format=LOG_FORMAT.format(classname))
        self.log = logging.getLogger(classname)
        self.log.info("Initializing")
        self.__numprocs = numprocs

    def update_xmls(self):
        from multiprocessing import Process, Queue
        import ftplib

        self.log.info("Downloading list of files")
        ftp = ftplib.FTP(URL_SIFTS_FTP, 'anonymous', '')
        ftp.cwd(PATH_SIFTS_FTP)
        filelist = ["{}/{}/{}".format(URL_SIFTS_FTP,PATH_SIFTS_FTP,r) for r in ftp.nlst()]
        q = Queue()

        i = 0
        for files in split_list(filelist,self.__numprocs):
            Process(target=download, args=(q,files,PATH_SIFTS_XML,i,self.log)).start()
            i+=1

    def update_derived(self):
        from multiprocessing import Process, Queue

        q = Queue()

        i = 0

        for files in split_list(URLS_SIFTS_DERIVED,self.__numprocs):
            Process(target=download, args=(q,files,PATH_SIFTS_DERIVED,i,self.log)).start()
            i+=1

class SiftsEntry(object):
    def __init__(self,id):
        self.__id = id
        self.__entities = []

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def entities(self):
        return self.__entities

    @entities.setter
    def entities(self, entities):
        self.__entities = entities

    def addentity(self,entity):
        self.__entities.append(entity)

    def __getitem__(self, index):
        return self.__entities[index]

    def __setitem__(self, index, value):
        self.__entities[index] = value

    def __repr__(self):
        return "SiftsEntry {0}".format(self.__id)

class SiftsEntity(object):
    def __init__(self,id,type):
        self.__type = type
        self.__id = id
        self.__segments = []

    @property
    def type(self):
        return self.__type

    @type.setter
    def type(self, type):
        self.__type = type

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def segments(self):
        return self.__segments

    @segments.setter
    def segments(self, segments):
        self.__segments = segments

    def addsegment(self, segment):
        self.__segments.append(segment)

    def __getitem__(self, index):
        return self.__segments[index]

    def __setitem__(self, index, value):
        self.__segments[index] = value

    def __repr__(self):
        return "SiftsEntity (id: {0}, type:{1})".format(self.__id,self.__type)

class SiftsSegment(object):
    def __init__(self, id, start, end):
        self.__id = id
        self.__start = start
        self.__end = end
        self.__residues = []

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def start(self):
        return self.__start

    @start.setter
    def start(self, start):
        self.__start = start

    @property
    def end(self):
        return self.__end

    @end.setter
    def end(self, end):
        self.__end = end

    @property
    def residues(self):
        return self.__residues

    @residues.setter
    def residues(self, residues):
        self.__residues = residues

    def addresidue(self, residue):
        self.__residues.append(residue)

    def __getitem__(self, index):
        return self.__residues[index]

    def __setitem__(self, index, value):
        self.__residues[index] = value

    def __repr__(self):
        return "SiftsSegment (id: {0}, start:{1}, end: {2})".format(self.__id, self.__start, self.__end)

class SiftsResidue(object):
    def __init__(self):
        self.__pdbresnum = ""
        self.__pdbresname = ""
        self.__chainid = ""
        self.__uniprotresname = ""
        self.__uniprotpos = None
        self.__naturalpos = None
        self.__seqresname = ""
        self.__pdbid = ""
        self.__uniprotaccession = ""
        self.__ss = ""
        self.__notobserved = False
        self.__conflict = False
        self.__modified = False

    @property
    def pdbresnum(self):
        return self.__pdbresnum

    @pdbresnum.setter
    def pdbresnum(self, pdbresnum):
        self.__pdbresnum = pdbresnum

    @property
    def pdbresname(self):
        return self.__pdbresname

    @pdbresname.setter
    def pdbresname(self, pdbresname):
        self.__pdbresname = pdbresname

    @property
    def chainid(self):
        return self.__chainid

    @chainid.setter
    def chainid(self, chainid):
        self.__chainid = chainid

    @property
    def uniprotresname(self):
        return self.__uniprotresname

    @uniprotresname.setter
    def uniprotresname(self, uniprotresname):
        self.__uniprotresname = uniprotresname

    @property
    def uniprotpos(self):
        return self.__uniprotpos

    @uniprotpos.setter
    def uniprotpos(self, uniprotpos):
        self.__uniprotpos = uniprotpos

    @property
    def naturalpos(self):
        return self.__naturalpos

    @naturalpos.setter
    def naturalpos(self, naturalpos):
        self.__naturalpos = naturalpos

    @property
    def seqresname(self):
        return self.__seqresname

    @seqresname.setter
    def seqresname(self, seqresname):
        self.__seqresname = seqresname

    @property
    def pdbid(self):
        return self.__pdbid

    @pdbid.setter
    def pdbid(self, pdbid):
        self.__pdbid = pdbid

    @property
    def uniprotaccession(self):
        return self.__uniprotaccession

    @uniprotaccession.setter
    def uniprotaccession(self, uniprotaccession):
        self.__uniprotaccession = uniprotaccession

    @property
    def notobserved(self):
        return self.__notobserved

    @notobserved.setter
    def notobserved(self, notobserved):
        self.__notobserved = notobserved

    @property
    def ss(self):
        return self.__ss

    @ss.setter
    def ss(self, ss):
        self.__ss = ss

    @property
    def modified(self):
        return self.__modified

    @modified.setter
    def modified(self, modified):
        self.__modified = modified

    @property
    def conflict(self):
        return self.__conflict

    @conflict.setter
    def conflict(self, conflict):
        self.__conflict = conflict

class SiftsParsingError(Exception):
    pass

class SiftsParser(object):

    def __init__(self):
        self.__SIFTS_TAG_PREFIX = "{http://www.ebi.ac.uk/pdbe/docs/sifts/eFamily.xsd}"

    def parse(self,filepath):
        try:
            f = gzip.open(filepath)
        except IOError:
            raise SiftsParsingError("{0} does not exist".format(filepath))
        root = ET.parse(f).getroot()
        pdbid = root.attrib["dbAccessionId"]

        sen = SiftsEntry(pdbid)

        for entity in root.findall('{0}entity'.format(self.__SIFTS_TAG_PREFIX)):

            type = entity.attrib["type"]
            id = entity.attrib["entityId"]

            se = SiftsEntity(type,id)

            sen.addentity(se)

            for segment in entity.findall("{0}segment".format(self.__SIFTS_TAG_PREFIX)):

                id = segment.attrib["segId"]
                start = segment.attrib["start"]
                end = segment.attrib["end"]

                ss = SiftsSegment(id,start,end)

                se.addsegment(ss)

                for residue in segment.findall("{0}listResidue/{0}residue".format(self.__SIFTS_TAG_PREFIX)):

                    sr = SiftsResidue()

                    ss.addresidue(sr)

                    sr.naturalpos = int(residue.attrib["dbResNum"])
                    sr.seqresname = residue.attrib["dbResName"]

                    for detail in residue.findall("{0}residueDetail".format(self.__SIFTS_TAG_PREFIX)):

                        if detail.attrib["property"] == "codeSecondaryStructure":
                            sr.ss = detail.text
                        elif detail.attrib["property"] == "Annotation":
                            ann = detail.text.strip().replace("\n          ","")
                            if ann == "Not_Observed":
                                sr.notobserved = True
                            elif ann == "Conflict":
                                sr.conflict = True
                            elif ann == "PDB modified":
                                sr.modified = True

                    links = residue.findall("{0}crossRefDb".format(self.__SIFTS_TAG_PREFIX))

                    for link in links:

                        dbsrc = link.attrib["dbSource"]
                        dbacc = link.attrib["dbAccessionId"]
                        dbresnum = link.attrib["dbResNum"]
                        dbresname = link.attrib["dbResName"]

                        if dbsrc == "PDB":
                            dbchainid = link.attrib["dbChainId"]

                            sr.pdbid = dbacc
                            sr.chainid = dbchainid
                            sr.pdbresnum = dbresnum
                            sr.pdbresname = dbresname
                        elif dbsrc == "UniProt": 
                            sr.uniprotaccession = dbacc
                            sr.uniprotpos = int(dbresnum)
                            sr.uniprotresname = dbresname

        return sen
