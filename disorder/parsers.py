# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.

import xml.etree.ElementTree as ET
import StringIO
import urllib2
from Bio import SeqIO
from itertools import tee
from biocomppy.seq import BioSeq
import re

class DisProt(object):
    def __init__(self,fileuri,version=None):
        self.__id = id
        self.__fileuri = fileuri
        self.__version = version
        self.__entries = []

    @property
    def version(self):
        return self.__version

    @version.setter
    def version(self, version):
        self.__version = version
        
    @property
    def fileuri(self):
        return self.__fileuri

    @fileuri.setter
    def fileuri(self, fileuri):
        self.__fileuri = fileuri

    @property
    def entries(self):
        return self.__entries

    @entries.setter
    def entries(self,entries):
        self.__entries = entries

    def addentry(self,entry):
        self.__entries.append(entry)

    def __getitem__(self, index):
        return self.__entries[index]

    def __setitem__(self, index, value):
        self.__entries[index] = value

    def __repr__(self):
        return "DisProt {0}".format(self.__version)

class DisProtEntry(object):
    def __init__(self,id,uacc,seq):
        self.__id = id
        self.__seq = seq
        self.__uacc = uacc
        self.__regions = []
        self.__uniprot_maps = []
        self.__comments = []

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id
        
    @property
    def uacc(self):
        return self.__uacc

    @uacc.setter
    def uacc(self, uacc):
        self.__uacc = uacc

    @property
    def seq(self):
        return self.__seq

    @seq.setter
    def seq(self, seq):
        self.__seq = seq

    @property
    def regions(self):
        return self.__regions

    @regions.setter
    def regions(self, regions):
        self.__regions = regions

    @property
    def comments(self):
        return self.__comments

    @comments.setter
    def comments(self, comments):
        self.__comments = comments

    @property
    def mapped_uniprot_entries(self):
        uacc = []
        for r in self.__regions:
            for m in r:
                uacc.append(m.uacc)

        return list(set(uacc))

    def addregion(self,region):
        self.__regions.append(region)

    def add_mapped_uniprot_entry(self,uacc,seq):
        self.__mapped_uniprot_entries.append({"acc":uacc,"seq":seq})

    def add_uniprot_map(self,uniprot_map):
        self.__uniprot_maps.append(uniprot_map)

    def addcomment(self,comment):
        self.__comments.append(comment)

    def __getitem__(self, index):
        return self.__regions[index]

    def __setitem__(self, index, value):
        self.__regions[index] = value

    def __repr__(self):
        return "DisProtEntry {0}".format(self.__id)

class DisProtRegion(object):
    def __init__(self,id,type,start,end,seq):
        self.__type = type
        self.__id = id
        self.__start = start
        self.__end = end
        self.__seq = seq
        self.__modtypes = []
        self.__structypes = []
        self.__funclasses = []
        self.__funsclasses = []
        self.__uniprot_maps = []

    @property
    def type(self):
        return self.__type

    @type.setter
    def type(self, type):
        self.__type = type

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def start(self):
        return self.__start

    @start.setter
    def start(self, start):
        self.__start = start

    @property
    def end(self):
        return self.__end

    @end.setter
    def end(self, end):
        self.__end = end

    @property
    def seq(self):
        return self.__seq

    @seq.setter
    def seq(self, seq):
        self.__seq = seq

    @property
    def funsclasses(self):
        return self.__funsclasses

    @property
    def funclasses(self):
        return self.__funclasses

    @property
    def structypes(self):
        return self.__structypes

    @property
    def modtypes(self):
        return self.__modtypes

    def add_funsclass(self,map):
        self.__funsclasses.append(map)

    def add_funclass(self,map):
        self.__funclasses.append(map)

    def add_structype(self,map):
        self.__structypes.append(map)

    def add_modtype(self,map):
        self.__modtypes.append(map)

    def add_uniprot_map(self,map):
        self.__uniprot_maps.append(map)

    def __getitem__(self, index):
        return self.__uniprot_maps[index]

    def __repr__(self):
        return "DisProtRegion (id: {0}, type:{1}, start:{2}, end:{3})".format(self.__id,self.__type,self.__start,self.__end)

class DisProtUniProtMap(object):
    def __init__(self,uacc,dstart,dend,ustart,uend,pctseqid):
        self.__uacc = uacc
        self.__dstart = dstart
        self.__dend = dend
        self.__ustart = ustart
        self.__uend = uend
        self.__pctseqid = pctseqid

    @property
    def uacc(self):
        return self.__uacc

    @uacc.setter
    def uacc(self, uacc):
        self.__uacc = uacc

    @property
    def dstart(self):
        return self.__dstart

    @dstart.setter
    def dstart(self, dstart):
        self.__dstart = dstart

    @property
    def dend(self):
        return self.__dend

    @dend.setter
    def dend(self, dend):
        self.__dend = dend

    @property
    def ustart(self):
        return self.__ustart

    @ustart.setter
    def ustart(self, ustart):
        self.__ustart = ustart

    @property
    def uend(self):
        return self.__uend

    @uend.setter
    def uend(self, uend):
        self.__uend = uend

    @property
    def pctseqid(self):
        return self.__pctseqid

    @pctseqid.setter
    def pctseqid(self, pctseqid):
        self.__pctseqid = pctseqid

    def __repr__(self):
        return "DisProtUniProtMap (uacc: {0} disprot:[{1}:{2}] uniprot:[{3}:{4}], pctseqid:{5})".format(self.__uacc, self.__dstart, self.__dend, self.__ustart, self.__uend, self.__pctseqid)

class DisProtParser(object):
    @staticmethod
    def parse(dpfile,version=None,get_uniprot_replacements=False):

        dp = DisProt(dpfile,version)

        with open(dpfile) as f:
            xmlstr = f.read()

        xmlstr = unicode(xmlstr,errors="ignore") #fix a series of xml aberrations in the disprot files
        xmlstr = xmlstr.replace("xmlns","ignore")
        xmlstr = xmlstr.replace("<br<br>","")
        xmlstr = xmlstr.replace("&","&amp;")
        xmlstr = xmlstr.replace("<br>","")
        xmlstr = xmlstr.replace("<b>","")
        xmlstr = xmlstr.replace("</br>","")
        xmlstr = xmlstr.replace("</b>","")
        xmlstr = xmlstr.replace("</b/>","")

        s = StringIO.StringIO(xmlstr)
        root = ET.parse(s)
        s.close()

        for protein in root.findall("protein"):
            regs = []
            seqmob = ""

            disprotseq = protein.find("general/sequence").text
            disid = protein.attrib["id"]
            uniprotid = protein.find("general/uniprot").text # the valid uniprotid (will change in the next lines if the entry has been replaced or deleted).
            uniprot_entries = None

            #if disid != 'DP00038':
            #    continue

            if uniprotid == 'N/A' or uniprotid == None:
                uniprotid = None
            else:
                if get_uniprot_replacements:
                    theurl = "http://www.uniprot.org/uniprot/?query=replaces:{0}&format=fasta".format(uniprotid)
                    funiprot = urllib2.urlopen(theurl)
                    uniprot_entries,uniprot_entries_forcount = tee(SeqIO.parse(funiprot, "fasta"))
    
                    if len(list(uniprot_entries_forcount)) == 0: #no replacements, let's go with the original
                        theurl = "http://www.uniprot.org/uniprot/{0}.fasta".format(uniprotid)
                        funiprot = urllib2.urlopen(theurl)
                        uniprot_entries = SeqIO.parse(funiprot, "fasta")
    
                    uniprot_entries = [[entry.id.split("|")[1],str(entry.seq)] for entry in uniprot_entries]

            dpe = DisProtEntry(disid,uniprotid,disprotseq)

            for comment in protein.findall("comments/comment"):
                dpe.addcomment(comment.text)

            for region in protein.findall("regions/region"):
                rid = region.attrib["id"]
                type = region[0].text
                rstart = int(region[2].text)
                rend = int(region[3].text)
                rseq = disprotseq[(rstart-1):rend]

                r = DisProtRegion(rid,type,rstart,rend,rseq)
                for modtype in region.findall("modification_types/modification_type"):
                    r.add_modtype(modtype.text)
                for structype in region.findall("structural_functional_types/structural_functional_type"):
                    r.add_structype(structype.text)
                for funclass in region.findall("functional_classes/functional_class"):
                    r.add_funclass(funclass.text)
                for funsclass in region.findall("functional_subclasses/functional_subclass"):
                    r.add_funsclass(funsclass.text)
                dpe.addregion(r)

            if uniprot_entries:
                for uniprot_entry in uniprot_entries:
                    uacc = uniprot_entry[0]
                    uniprotseq = uniprot_entry[1]

                    for region in dpe:
                        rseq = region.seq
                        ann = "O" if region.type == "Ordered" else "D"

                        aligned_useq,aligned_rseq = BioSeq.align(uniprotseq,rseq)
                        aligned_aseq = re.sub(r'[A-Z]',ann,aligned_rseq)

                        regmaps = BioSeq.get_regions_maps(aligned_useq,aligned_rseq,seq2_offset=region.start)

                        for m in regmaps:
                            uregs= m[0][0]
                            urege= m[0][1]
                            dregs= m[1][0]
                            drege= m[1][1]
                            uregseq = uniprotseq[uregs-1:urege]
                            dregseq = disprotseq[dregs-1:drege]
                            pctmatch,matches,length = BioSeq.compare(uregseq,dregseq)

                            region.add_uniprot_map(DisProtUniProtMap(uacc,dregs,drege,uregs,urege,pctmatch))

            dp.addentry(dpe)

        return(dp)

class IDEAL(object):
    def __init__(self,fileuri,version=None):
        self.__id = id
        self.__fileuri = fileuri
        self.__version = version
        self.__entries = []

    @property
    def version(self):
        return self.__version

    @version.setter
    def version(self, version):
        self.__version = version
        
    @property
    def fileuri(self):
        return self.__fileuri

    @fileuri.setter
    def fileuri(self, fileuri):
        self.__fileuri = fileuri

    @property
    def entries(self):
        return self.__entries

    @entries.setter
    def entries(self, entries):
        self.__entries = entries

    def addentry(self,entry):
        self.__entries.append(entry)

    def __getitem__(self, index):
        return self.__entries[index]

    def __setitem__(self, index, value):
        self.__entries[index] = value

    def __repr__(self):
        return "IDEAL {0}".format(self.__version)

class IDEALEntry(object):
    def __init__(self,id):
        self.__id = id
        self.__conditions = []

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def conditions(self):
        return self.__conditions

    @conditions.setter
    def conditions(self, conditions):
        self.__conditions = conditions

    def addcondition(self,condition):
        self.__conditions.append(condition)

    def __getitem__(self, index):
        return self.__conditions[index]

    def __setitem__(self, index, value):
        self.__conditions[index] = value

    def __repr__(self):
        return "IDEALEntry {0}".format(self.__id)

class IDEALCondition(object):
    def __init__(self,id,pdbid,pubmedid,method):
        self.__id = id
        self.__pubmedid = pubmedid
        self.__pdbid = pdbid
        self.__method = method
        self.__missings = []

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def method(self):
        return self.__method

    @method.setter
    def method(self, method):
        self.__method = method

    @property
    def pubmedid(self):
        return self.__pubmedid

    @pubmedid.setter
    def pubmedid(self, pubmedid):
        self.__pubmedid = pubmedid

    @property
    def pdbid(self):
        return self.__pdbid

    @pdbid.setter
    def pdbid(self, pdbid):
        self.__pdbid = pdbid

    def addmissing(self,missing):
        self.__missings.append(missing)

    def __getitem__(self, index):
        return self.__missings[index]

    def __setitem__(self, index, value):
        self.__missings[index] = value

    def __repr__(self):
        return "IDEALCondition {0}".format(self.__id)

class IDEALMissing(object):
    def __init__(self,chain):
        self.__chain = chain
        self.__locations = []

    @property
    def chain(self):
        return self.__chain

    @chain.setter
    def chain(self, chain):
        self.__chain = chain

    def addlocation(self,location):
        self.__locations.append(location)

    def __getitem__(self, index):
        return self.__locations[index]

    def __setitem__(self, index, value):
        self.__locations[index] = value

    def __repr__(self):
        return "IDEALMissing {0}".format(self.__chain)

class IDEALLocation(object):
    def __init__(self,start,end):
        self.__start = start
        self.__end = end

    @property
    def start(self):
        return self.__start

    @start.setter
    def start(self, start):
        self.__start = start

    @property
    def end(self):
        return self.__end

    @end.setter
    def end(self, end):
        self.__end = end

    def __repr__(self):
        return "IDEALLocation {0}-{1}".format(self.__start,self.__end)

class IDEALParser(object):
    @staticmethod
    def parse(fileuri,version=None):

        ideal = IDEAL(fileuri,version)

        with open(fileuri) as f:
            xmlstr = f.read()

        s = StringIO.StringIO(xmlstr)
        root = ET.parse(s)
        s.close()

        for protein in root.findall("IDEAL_entry"):
            idealid = protein.find("idp_id").text

            ie = IDEALEntry(idealid)
            ideal.addentry(ie)

            for condition in protein.findall("Condition"):
                cid = condition.find("condition_id").text
                cpdbid = condition.find("pdb_id").text
                cmethod = condition.find("method").text
                cpubmedid = condition.find("condition_reference")[0].text

                ic = IDEALCondition(cid,cpdbid,cpubmedid,cmethod)
                ie.addcondition(ic)

                for missing in condition.findall("missing"):

                    if missing.find("sws_end").text == '0':
                        continue

                    mchain = missing.find("chain").text
                    im = IDEALMissing(mchain)
                    ic.addmissing(im)

                    for loc in missing.findall("missing_location"):
                        lstart = int(loc[0].text)
                        lend = int(loc[1].text)

                        il = IDEALLocation(lstart,lend)
                        im.addlocation(il)
        return(ideal)
