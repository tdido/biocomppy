# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.

import os,glob,gzip,tempfile,shlex,subprocess,itertools,imp
from csb.bio.io.wwpdb import StructureParser,StructureFormatError,FileSystemStructureProvider,StructureNotFoundError
from csb.io import TempFile
from Bio import SeqIO

def grouper(n, iterable, fillvalue=None):
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return itertools.izip_longest(*args, fillvalue=fillvalue)

class GzipStructureProvider(FileSystemStructureProvider):

    def get(self, id, model=None):
        self._templates = ['pdb{id}.ent.gz']
        
        pdb = self.find(id)
        
        if pdb is None:
            raise StructureNotFoundError(id)
        else:
            with TempFile() as temp:
                gzipf = gzip.open(pdb)
                text = gzip.open(pdb).read()  # consider wrapping this in a try..finally to close the gzip
                temp.write(text)
                temp.flush()
                gzipf.close()
                return StructureParser(temp.name).parse_models()

class MobiError(Exception):
    pass

class MobiWrapper:
    def __init__(self,force_mobi=False,path_stage="/tmp"):
        self.__force_mobi = force_mobi
        self.__PATH_STAGE = path_stage

    def __trapmobinums(self,v):
        try:
          i = float(v)
        except ValueError:
            return(v)
        else:
            return("-")

    def __processmobioutfile(self,fname):
        tmpl = fname.split("/")
        pdbid = tmpl[-2]
        pdbchain = tmpl[-1].split("_")[1].split(".")[0]
            
        pdbgroup = pdbid[1:3]

        try:
            with open(fname,"r") as f:
                f = list(f)
                annseq = f[1].rstrip("\n")
                mobseq = f[2].rstrip("\n")
        
            return(pdbchain,annseq,mobseq)
        except IOError:
            print fname + " not found (probably not multimodel)"
            return([None] * 3)

    def getannotation(self,pdbid,pdbfilepath):

        pdbgroup = pdbid[1:3]

        thedir = "{0}/{1}/{2}".format(self.__PATH_STAGE,pdbgroup,pdbid)

        mobioutfiles = glob.glob("{0}/{1}/{2}/out_*.fasta".format(self.__PATH_STAGE,pdbgroup,pdbid))

        if not mobioutfiles or self.__force_mobi:
            try:
                sp = GzipStructureProvider(paths=pdbfilepath) 
            except StructureNotFoundError:
                raise
            except StructureFormatError:
                raise

            models = sp.get(pdbid)

            if len(models) > 1:
                self.__run_mobi(pdbid,pdbfilepath,thedir)
            else:
                raise MobiError("{0} is not multi model, skipping".format(pdbid))

        mobioutfiles = glob.glob("{0}/{1}/{2}/out_*.fasta".format(self.__PATH_STAGE,pdbgroup,pdbid))

        if mobioutfiles:
            chains = {}
            for mobioutfile in mobioutfiles:
                pdbchain,annseq,tarseq = self.__processmobioutfile(mobioutfile)

                if not annseq:
                    raise MobiError("No annseq in {0}".format(mobioutfile))

                chains[pdbchain] = [annseq,tarseq]

            return chains
        else:
            raise MobiError("MOBI output for {0} did not contain out_*.fasta files...".format(pdbid))

    def __run_mobi(self,pdbid,pdbzfile_path,thedir):
        import shutil,subprocess

        thefile = "{0}/pdb{1}.ent.gz".format(pdbzfile_path,pdbid)

        pdbgroup = pdbid[1:3]
        try:
            os.makedirs(thedir)
        except OSError:
            shutil.rmtree(thedir)
            os.makedirs(thedir)

        try:
            (file,modpath,desc) = imp.find_module("biocomppy")
            binpath = "{0}/ext_bin/mobi/bin".format(modpath)

            shutil.copy("{0}/pdbnmr2mov.pl".format(binpath),thedir)
            shutil.copy(thefile,thedir)
            subprocess.call(["gunzip","{0}/pdb{1}.ent.gz".format(thedir,pdbid)])
            subprocess.call(["mv","{0}/pdb{1}.ent".format(thedir,pdbid),"{0}/{1}.pdb".format(thedir,pdbid)])
            subprocess.call(["perl","{0}/pdbnmr2mov.pl".format(thedir),"{0}/{1}.pdb".format(thedir,pdbid),"."],cwd=thedir)
        except IOError:
            raise MobiError("Couldn't run MOBI. Check that the scripts/binaries are accessible at {0}/ext_bin/mobi/bin".format(modpath))
