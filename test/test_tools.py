# Copyright 2012-2019 Tomas Di Domenico
# 
# This file is part of Biocomppy.
# 
# Biocomppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Biocomppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Biocomppy.  If not, see <http://www.gnu.org/licenses/>.

'''
    This file is part of the MobiDB analysis suite.

    The MobiDB analysis suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import unittest,os
from biocomppy.tools import strcompare

class TestTools(unittest.TestCase):

    '''@classmethod
    def setUpClass(cls):
        dc = discons.DisConsensus()
        dc.annfilepath = os.path.split(os.path.realpath(__file__))[0] + "/data/xp/annotations.fasta"
        dc.calculate()
        cls.levels = dc.levels
        cls.scores = dc.scores
    '''
    def test_scores(self):
        str1 = 'AAA--'
        str2 = 'AC-A-'

        self.assertEqual(strcompare(str1,str2,reqmarker='any',gapsmatch=False), (0.25,1,4))
        self.assertEqual(strcompare(str1,str2,reqmarker='left',gapsmatch=False), (float(1)/3,1,3))
        self.assertEqual(strcompare(str1,str2,reqmarker='right',gapsmatch=False), (float(1)/3,1,3))
        self.assertEqual(strcompare(str1,str2,reqmarker='both',gapsmatch=False), (0.5,1,2))

        self.assertEqual(strcompare(str1,str2,reqmarker='any',gapsmatch=True), (float(2)/5,2,5))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestTools)
    unittest.TextTestRunner(verbosity=2).run(suite)
